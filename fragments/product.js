module.exports =
`uid
sku_id
parent_sku_id
product_name
display_name
display_name_detail
color
image_cover
image_banner
image_highlight
instock
show_detail
short_desc
promotion_desc
keywords
viewcount
ribbon
discount_tag
free_shipping_tag
count_details: count(details)
is_disabled_cod
is_temporary
trailer
original
detail {
  expand(_all_)
}
product.pricing (first: -1) {
  price_with_vat
  listed_price_with_vat
  shipping_fee
}
product.manufacturer {
  uid
  manufacturer_name
}
product.supplier: product.partner {
  uid
  supplier_name: partner_name
  supplier_id: id
  required_order_value
  shipping_time
}
product.collection @filter((NOT eq(is_deleted, true)) AND eq(collection_type, 0)) {
  uid
  collection_name
  collection_image
}
product.brand {
  uid
  brand_name
}
autoplay_video
previews {
  expand(_all_)
}
video_transcodes {
  expand(_all_)
}
areas: product.areas {
  uid
  name
  vn_all_province
}
product.areas {
  uid
  name
  vn_all_province
}
display_bill_name
short_des_1_show
short_des_1
short_des_1_color
short_des_2_show
short_des_2
short_des_2_color
short_des_3_show
short_des_3
short_des_3_color
short_des_4_show
short_des_4
short_des_4_color
description_html
product.tag {
  uid
  tag_title
  tag_category
  tag_type
  assess_value
  promotion_value@facets(purchase:Purchase, extra:Extra, percentage: Percentage, fee:Fee)
  display_status
  shipping_value {
    uid
    name
  }
  image_promotion
  name_tag_value_1
  name_tag_value_2
}
short_descriptions {
  uid
  display_name
  short_des_1
  short_des_1_color
  short_des_2
  short_des_2_color
  short_des_3
  short_des_3_color
  short_des_4
  short_des_4_color
}
short_description_1 {
  uid
  short_des
  short_des_color
}
short_description_2 {
  uid
  short_des
  short_des_color
}
short_description_3 {
   uid
  short_des
  short_des_color
}
short_description_4 {
  uid
  short_des
  short_des_color
}

brand_shop: ~brand_shop.product {
  uid
  brand_shop_name
  display_name
  display_name_detail
  display_name_in_product
  hotline
  certification_tag
  image_banner
  image_logo
  image_details {
    uid
    media_type
    source
    display_order
  }
  description_html
  brand_shop.product
  product_counts: count(brand_shop.product)
  display_status
  created_at
  is_deleted
  updated_at
  skin_name
  skin_image
  skin_display_at_home
  skin_display_at_brandshop
}
is_disabled_quickpay
shelf_life
warranty_policy
`