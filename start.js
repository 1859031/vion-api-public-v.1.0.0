"use strict"

const build = require('./server')

function start () {

  const app = build()

  function onBeforeExit (name, code) {
    if(!process.exiting) {
      process.exiting = true
      console.log('>>>', 'Exiting', '/', name, code, '<<<')
      app.close(() => {
        console.log('Fastify closed')
        process.exit()
      })
    }
  }

  app.listen(process.env.PORT, process.env.ADDR)
    .then(address => {
      console.log("App listening:", address)
      console.log("Process ID:", process.pid)
      for(let ev of ['SIGTERM', 'SIGINT', 'SIGTSTP', 'SIGQUIT']) {
        process.on(ev, onBeforeExit)
      }
    })
    .catch(err => {
      console.log(err)
      process.exit(1)
    })
}

start()