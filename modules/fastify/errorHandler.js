function parseBody (body, length) {
  if(length > 1024) {
    if(typeof body === 'object') {
      return JSON.stringify(body).slice(0, 1024) + '...'
    } else if(typeof body === 'string') {
      return body.slice(0, 1024)
    }
  }
  return body
}

module.exports = function (error, {client = "someone", method, url, params, query, body, headers}, reply) {
  if(reply.statusCode === 401) {
    if("$username" in body) {
      console.log("!!! LOGIN FAILED", body["$username"])
    }
    reply.send(error)
    return
  }

  if(error.validation && logLevel > 0) { // Tắt các log validation đi
    reply.send(new Error("validation failed!"))
    return
  }

  console.log(`>>> Request: (${client})`, method, url)

  const level = reply.statusCode < 500 ? 'Warn' : 'Error'

  if(error instanceof Error) {
    // error.stack thì có chứa cả message trong đó
    // nhưng error.message chỉ có message thôi
    console.log(`-   ${level}|`, error.validation ? error.message : error.stack)
  } else {
    console.log(`-   ${level}|`, error)
  }
  if(params && Object.keys(params).length) {
    console.log('-   Params:', params)
  }
  if(query && Object.keys(query).length) {
    console.log('-   Query:', query)
  }
  if(body) {
    const length = +headers['content-length']
    console.log(`-   Body (${length} bytes):`, parseBody(body, length))
  }

  if(error.validation) {
    reply.send(new Error("validation failed!"))
  } else {
    reply.send(error)
  }
}