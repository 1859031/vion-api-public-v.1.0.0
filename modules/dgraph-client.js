"use strict"

const dgraph = require("dgraph-js")

const {
  DgraphClientStub,
  DgraphClient,
  Mutation,
  Request,
  Operation
} = dgraph

function createStub (host) {
  if(typeof host === 'string') {
    return new DgraphClientStub(host)
  } else {
    const { addr, ca, key, cert } = host
    return new DgraphClientStub(addr, dgraph.grpc.credentials.createSsl(ca, key, cert))
  }
}

function createClient (hosts) {
  if(!hosts || hosts.length === 0) {
    throw new Error("no alpha hosts")
  }
  const stubs = hosts.map(createStub)
  // connect multi clusters
  const queryClient = new DgraphClient(...stubs)
  const mutateClient = new DgraphClient(stubs[0])

  /**
    * Make query Dgraph GraphQL+-
    * @param {String} query `all(func: ...)` | `query all($a: string) { all(func: eq(name, $my_var)) { name } }`
    * @param {Object} vars { "$my_var": "LPN Nha" }
    * @returns {Promise}
    */
  async function query (...args) {
    return (await queryClient.newTxn().queryWithVars(...args)).getJson()
  }

  async function txnQuery (txn, ...args) {
    return (await txn.queryWithVars(...args)).getJson()
  }

  /**
    * Update & delete, Update | delete
    * @param {Object} opts { del: {...}, set: {...} }
    * @returns {Mutation}
    */
  function createMutation (opts) {
    const m = new Mutation()
    if(opts.del) {
      typeof opts.del === 'string' ? m.setDelNquads(opts.del) : m.setDeleteJson(opts.del)
    }
    if(opts.set) {
      typeof opts.del === 'string' ? m.setSetNquads(opts.set) : m.setSetJson(opts.set)
    }
    return m
  }

  /**
    * Tạo request chứa nhiều mutate trong 1 lần query
    * @param {Array} mutations [{del: {...}, set: {...}}, { del: {...}, set: {...} }]
    * @returns {Request}
    */
  function createRequest (mutations) {
    const req = new Request()
    if(mutations) {
      req.setMutationsList(Array.isArray(mutations) ? mutations : [mutations])
    }
    return req
  }

  /**
    * Update & delete, Update | delete
    * @param {Object} opts { del: {...}, set: {...} }
    * @param {Transaction} txn (Optional)
    * @returns {Promise}
    * @example
    * mutate( { del: {...}, set: {...} })
    */
  async function mutate (opts, txn) {
    const m = createMutation(opts)
    if(!txn) {
      m.setCommitNow(true)
      txn = mutateClient.newTxn()
    }
    return await txn.mutate(m)
  }

  /**
    * Thực thi nhiều mutation trong 1 lần gọi
    * @param {Array} mutations
    * @param {Transaction} txn (Optional)
    * @returns {Promise}
    */
  async function mutates (mutations, txn) {
    const req = createRequest(mutations.map(createMutation))
    if(!txn) {
      req.setCommitNow(true)
      txn = mutateClient.newTxn()
    }
    return await txn.doRequest(req)
  }

  /**
    * Query lấy thông số và Update, Delete dựa vào thông số của query
    * @param {Object} opts
    * @param {Object} txn (Optional) Transaction object
    * @returns {Promise}
    * tham khảo upsert
    * link: https://docs.dgraph.io/mutations/#upsert-block
    * example: upsert({ query: `v as var(func: eq(email: "ngocnha@gmail.com"))`, set: { uid: uid(v), email: ngocnha2@gmail.com } })
    */
  async function upsert (opts, txn) {
    const m = new Mutation()
    const req = new Request()
    if(opts.query) {
      req.setQuery(opts.query)
    }
    if(opts.cond) {
      m.setCond(opts.cond)
    }
    if(opts.del) {
      m.setDeleteJson(opts.del)
    }
    if(opts.set) {
      m.setSetJson(opts.set)
    }
    req.addMutations(m)
    if(!txn) {
      req.setCommitNow(true)
      txn = mutateClient.newTxn()
    }
    return await txn.doRequest(req)
  }
  // Các operation phải thực hiện tuần tự,
  // thực hiện chung vừa dropAll vừa alter trong 1 lần sẽ gây sai sót

  function dropAll () {
    const o = new Operation()
    o.setDropAll(true)
    return mutateClient.alter(o)
  }
  function alter (schema) {
    const o = new Operation()
    o.setSchema(schema)
    return mutateClient.alter(o)
  }

  function close () {
    for(let stub of stubs) {
      stub.close()
    }
  }

  return {
    close,
    txn () {
      return mutateClient.newTxn()
    },
    dropAll,
    alter,
    query,
    txnQuery,
    createMutation,
    createRequest,
    mutate,
    mutates,
    upsert
  }
}

let services = {}

function getService (name, hosts) {
  if(typeof services[name] === 'object') {
    return services[name]
  } else if(hosts.length > 0) {
    return services[name] = createClient(hosts)
  }
  throw new Error('Required hosts')
}
function closeService (name) {
  services[name].close()
  delete services[name]
}
function closeAllServices () {
  for(let service of Object.values(services)) {
    service.close()
  }
  services = {}
  log.info('closed all db connection')
}

module.exports = {
  services,
  getService,
  closeService,
  closeAllServices
}
