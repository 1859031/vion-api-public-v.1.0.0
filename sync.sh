PS3="Chọn 1 trong: "
options=("> close <" "development" "production")
select menu in "${options[@]}";
do
  case $REPLY in
    1)
      exit 0
      ;;
    2)
      E="dev"
      ;;
    3)
      E="prod"
      ;;
  esac
  echo -e "\nSelect sync ($menu)"
  # v verbose, c checksum, r recursive
  rsync -vcr --include='fragments/***' --include='modules/***' --include='routes/***' --include='schemas/***' --include='services/***' --include='utils/***' --exclude '*' ./ ngocnha:~/projects/${E}/${E}_public_api/
  exit 0
done