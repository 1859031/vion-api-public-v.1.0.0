// Documents:
//   https://www.fastify.io/docs/v2.12.x/Validation-and-Serialization/
//   https://json-schema.org/understanding-json-schema/index.html
//   https://json-schema.org/understanding-json-schema/reference/regular_expressions.html#regular-expressions

module.exports = {
  matchOne (type, cases) {
    return { type, enum: cases }
  },
  array (...args) {
    if(args.length === 2) {
      const [minItems, items] = args
      return { type: 'array', minItems, items }
    }
    if(args.length === 1) {
      if(typeof args[0] === 'number') {
        return { type: 'array', minItems: args[0] }
      } else {
        return { type: 'array', items: args[0] }
      }
    }
  },
  uid: {
    type: 'string',
    pattern: "^0[xX][a-fA-F0-9]+$"
  },
  subOrderId: {
    type: 'string',
    pattern: "^S\\d{6}-\\d{6}[A-Z]{1,2}$"
  },
  primaryOrderId: {
    type: 'string',
    pattern: "^S\\d{6}-\\d{6}$"
  },
  orderStatus: {
    type: 'number',
    maximum: 9 // 0-9
  },
  stringNumber: {
    // nếu dùng number vẫn sẽ validate đúng chuỗi số sẽ bị convert lại mất số 0 trên đầu
    // dùng stringNumber để giữ số 0 cho chuỗi số
    type: 'string', pattern: "^\\d+$"
  },
  macAddress: {
    type: 'string',
    pattern: "^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$"
  },
  transactionId: {
    type: 'string',
    maxLength: 31,
    pattern: "^\\d+-\\S{10}$"
  },
  email: {
    type: 'string',
    pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
  },
}