const path = require('path')

const public_dir = path.join(rootDir, 'public')

const addr = process.env.ADDR ?? '0.0.0.0'
const port = process.env.PORT ?? 60000

const domain = "localhost"

const baseUrl = `http://${domain}:${port}`

const origin = baseUrl
const image_url = baseUrl
const video_url = baseUrl
const apiPrefix = "/api/v1.1"

module.exports = {
  domain,
  dirs: {
    public_dir
  },
  serve: {
    fastify_options: {
      logger: false
    },
    addr,
    port,
    origin,
    apiPrefix,
    image_url,
    video_url
  },
  services: {
    // alpha_grpc: require("./alpha"),
    alpha_grpc: ["alpha:9080"],
    redis: {
      host: "redis-vionmart",
      prefix: 'stv'
    },
    tvc_api: "http://localhost:61000"
  }
}
