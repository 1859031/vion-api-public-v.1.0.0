"use strict"

require('./globals')

const Fastify = require('fastify')

const {
  apiPrefix,
  fastify_options
} = config.serve

module.exports = function (prefix = apiPrefix, options = fastify_options) {
  const fastify = Fastify(options)

  fastify.setErrorHandler(require('./modules/fastify/errorHandler'))

  if(app_env !== 'production') { // Ở production cors đã có reverse proxy lo
    fastify.register(require('fastify-cors'), {
      origin: "*",
      allowedHeaders: "*",
      methods: ['GET', 'POST', 'PUT', 'DELETE, OPTIONS']
    })
  }

  fastify.register(require('fastify-static'), {
    root: config.dirs.public_dir,
    prefix: '/'
  })

  if(app_env === 'production') { // Chỉ cache trên production
    // Phải để cái này sau static, để khỏi cache route của static
    fastify.addHook('onRoute', require('./modules/fastify/cachePlugin'))
  }

  fastify.register(require('./routes'), {prefix})

  return fastify
}