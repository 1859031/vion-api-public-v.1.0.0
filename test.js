'use strict'

const app = require('./server')('/api')

async function test () {
  /* await request('post', '/api/makePayment', {
    "pay_gateway": "viettel",
    "amount": 20000,
    "customer_id": "0x9c5f",
    "order_id": "0xc363",
    "platform": "test"
  }) */
  await request('post', '/api/testCheckTransactionStatus', {
    "method": "viettel",
    "order_id": "0xc363"
  })
}

async function request (method, url, body) {
  let response
  const options = {
    method: method.toUpperCase(),
    url
  }
  if(body) {
    Object.assign(options, {
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(body)
    })
  }

  response = await app.inject(options)

  console.log('status code: ', response.statusCode)
  console.log('body: ', response.body)
}

test().then(() => {
  process.exit()
}).catch(err => {
  console.log(err)
})