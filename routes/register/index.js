"use strict"

const {registerGGStep1, verifyGGOtp, updateGGPwd, LoginGG} = require("./google")
const {registerPhoneStep1, verifyPhoneOtp, updatePhonePwd, LoginPhone} = require("./phone")

module.exports = [
  ['post', '/register/google/step-1', registerGGStep1],
  ['post', '/register/google/step-2', verifyGGOtp],
  ['post', '/register/google/step-3', updateGGPwd],
  ['post', '/login/google', LoginGG],
  ['post', '/login/phone', LoginPhone],
  ['post', '/register/phone/step-1', registerPhoneStep1],
  ['post', '/register/phone/step-2', verifyPhoneOtp],
  ['post', '/register/phone/step-3', updatePhonePwd],
]