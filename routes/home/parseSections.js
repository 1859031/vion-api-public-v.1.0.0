const db = mrequire('./services/tvcdb')
const prodFields = mrequire('./fragments/product')
const prodFilter = mrequire('./fragments/productFilter')
const { isUid, safeParseJson } = mrequire('./utils')
const {filterCollCantShow} = require('./filterItems')
const parseProduct = mrequire('./services/product-parser')

const collectionFields =
`uid
collection_name
collection_image
highlight_name
collection_icon
color_title
gradient_start
gradient_end
layout_type`

const collectionQuery = `{
  result(func: eq(collection_type, 0), orderasc: display_order) @filter(NOT eq(is_deleted, true) AND (has(~parent) AND eq(display_status, 2) AND (NOT eq(is_temporary, true)) ) ) {
    ${collectionFields}
    children: ~parent @filter(NOT eq(is_deleted, true)) {
      ${collectionFields}
      items: ~product.collection (first: 20, offset: 0) @facets(orderasc: display_order) ${prodFilter} {
        uid
        product_name
      }
    }
  }
}`

const collectionTempQuery = `query result($section_ref: string, $section_limit: string) {
  result(func: uid($section_ref), orderasc: display_order) @filter(eq(collection_type, 0) AND NOT (has(parent) OR eq(is_deleted, true)) AND ( eq(display_status, 2) AND ( eq(is_temporary, true)) ) ) {
    ${collectionFields}
    items: highlight.products (first: $section_limit, offset: 0) @facets(orderasc: display_order) @filter(eq(display_status, 2) AND NOT eq(is_deleted, true) AND NOT eq(is_temporary, true)) {
      ${prodFields}
    }
  }
}`
const livestreamGroupFields = `
uid
livestream_group_name
livestream_group.videostreams {
  uid
  stream.name
}
`
const videoStreamFields =
`uid
stream.name
stream.display_name
stream.start_time
stream.end_time
stream.type
stream.video
stream.image_thumb
stream.image_fullscreen
stream.view_count
stream.view_virtual
stream.is_highlight
stream.video_transcode
is_portrait`

const videoStreamQuery = `query result($section_ref: string, $time: int) {
  result(func: type($section_ref), orderasc: stream.start_time, first: 20, offset: 0) @filter( eq(display_status, 2) AND lt(stream.start_time, $time) AND (NOT HAS(stream.end_time) OR gt(stream.end_time, $time))) {
    ${videoStreamFields}
  }
  endResult(func: type($section_ref), orderdesc: stream.start_time, first: 20, offset: 0) @filter( eq(display_status, 2) AND lt(stream.end_time, $time)) {
    ${videoStreamFields}
  }
}`
const videoStreamGroupQuery = `query result($section_ref: string, $time: int) {
  livestreamGroup(func: uid($section_ref)) @filter(eq(display_status,2)) {
    result: livestream_group.videostreams @facets(orderasc: display_order) @filter( eq(display_status, 2) AND lt(stream.start_time, $time) AND (NOT HAS(stream.end_time) OR gt(stream.end_time, $time))) {
      ${videoStreamFields}
    }
    endResult: livestream_group.videostreams @facets(orderasc: display_order) @filter( eq(display_status, 2) AND lt(stream.end_time, $time)) {
      ${videoStreamFields}
    }
  }
}`
const endStreamQuery = `query result($section_ref: string, $time: int) {
  result(func: type($section_ref), orderdesc: stream.start_time, first: 20, offset: 0) @filter( eq(display_status, 2) AND lt(stream.end_time, $time)) {
    ${videoStreamFields}
  }
}`

const favouriteQuery = `query result($customer_id: int) {
  var(func: uid($customer_id)) {
    c_p as customer.favourite_products ${prodFilter}
  }

  result(func: uid(c_p)) ${prodFilter} {
    ${prodFields}
  }
}`

const brandShopFrag =
`uid
brand_shop_name
display_name
display_name_detail
hotline
certification_tag
image_banner
image_logo
image_details {
  uid
  media_type
  source
  display_order
}
description
brand_shop.product
product_counts: count(brand_shop.product)
display_status
created_at
is_deleted
updated_at`

const brandshopGroupQuery = (customer_id, brandshop_group_uid) => {
  if(!customer_id) {
    return `{
      result(func: uid(${brandshop_group_uid})) @filter(eq(display_status, 2)) {
        brandshop_group.brandshops @facets(orderasc: display_order) @filter(has(brand_shop.layout) and not eq(is_deleted, true) and eq(display_status, 2)) {
          ${brandShopFrag}
        }
      }
    }`
  }
  return `{
    var(func: uid(${customer_id})) {
      uid
      group_cus as ~group_customer.customers
      lay_cus as ~layout.customers @filter(not eq(is_deleted, true) AND eq(display_status, 2) AND eq(target_type, 1))
    }
    var(func: uid(group_cus)) {
      lay_group as ~layout.group_customers @filter(not eq(is_deleted, true) AND eq(display_status, 2) AND eq(target_type, 0))
    }
    lay_all as allCustomer(func: type(Layout)) @filter(not eq(is_deleted, true) AND eq(target_type, 2) AND eq(display_status, 2))
    var(func: uid(lay_cus, lay_group,lay_all)) @filter(has(~brand_shop.layout) and not eq(is_deleted, true) and eq(display_status, 2)){
      c as ~brand_shop.layout
    }
    result(func: uid(${brandshop_group_uid})) {
      brandshop_group.brandshops @facets(orderasc: display_order) @filter(uid(c) AND eq(display_status, 2) AND NOT eq(is_deleted, true)) {
        ${brandShopFrag}
      }
    }
  }`
}
const promotionFields = `
uid
  display_name
  display_name_detail
  from_date
  to_date
  description_html
  image_banner
  image_cover
  promotion.brand_shop{
    uid
    brand_shop_name
  }
  promotion.button_direct{
    uid
    link_name
    link_direct
    link_value
    link_action
  }
  promotion.promotion_value{
    uid
    promotion_name
    promotion_type
    promotion_values
    condition_value
    product_extra{
      uid
      product_name
    }
  }
  display_status
  promotion.products{
    uid
    product_name
  }
  promotion.product_exclude{
    uid
    product_name
  }
  config_type
  is_display
`
const promotionQuery = `{
  result(func: type(Promotions), orderasc: display_order) @filter(NOT has(promotion.brand_shop) AND NOT eq(is_deleted, true) AND eq(display_status, 2)) {
    ${promotionFields}
  }
}`
async function parseSections (data, uids, customer_id) {

  if(data?.['layout.layout_section']?.length) {
    let sections = data['layout.layout_section']
    let i = 0
    while (i !== sections.length) {
      let s = sections[i]
      if (s.section_type && s.section_type === 'dgraph.type') {
        if (s.section_value === 'collection') {
          const {result} = await db.query(collectionQuery)
          if (result?.length) {
            s[s.section_value] = filterCollCantShow(result)
          }
        } else if (s.section_value === 'livestream') {
          let params = {
            section_ref: s.section_ref,
            time: String(new Date().getTime())
          }
          let {livestreamGroup} = await db.query(videoStreamGroupQuery, {
            $section_ref: s.section_ref,
            $time: String(new Date().getTime())
          })
          let result = [], endResult = []
          if (livestreamGroup?.[0]) {
            result = livestreamGroup?.[0]?.result || []
            endResult = livestreamGroup?.[0]?.endResult || []
          }
          if (result?.length) {
            result.map(videoStream => {
              //Trạng thái live stream
              const now = new Date().getTime();
              if (!videoStream?.['stream.start_time'] || now < videoStream?.['stream.start_time']) {
                videoStream['stream.status'] = 0;
              } else if (!videoStream?.['stream.end_time'] || videoStream?.['stream.end_time'] >= now) {
                videoStream['stream.status'] = 1;
              } else {
                videoStream['stream.status'] = 2;
              }
            })
            s[s.section_value] = result.filter(c => c?.['stream.status'] !== 2 || c?.['stream.video_transcode']?.length > 0)
          }
          if (endResult?.length) {
            endResult.map(videoStream => {
              //Trạng thái live stream
              const now = new Date().getTime();
              if (!videoStream?.['stream.start_time'] || now < videoStream?.['stream.start_time']) {
                videoStream['stream.status'] = 0;
              } else if (!videoStream?.['stream.end_time'] || videoStream?.['stream.end_time'] >= now) {
                videoStream['stream.status'] = 1;
              } else {
                videoStream['stream.status'] = 2;
              }
            })
            s[s.section_value] = result.concat(endResult.filter(c => c?.['stream.status'] !== 2 || c?.['stream.video_transcode']?.length > 0))
          }
        } else if (s.section_value === 'end_stream') {
          const {result} = await db.query(endStreamQuery, {
            $section_ref: s.section_ref,
            $time: String(new Date().getTime())
          })
          if (result?.length) {
            result.map(videoStream => {
              //Trạng thái live stream
              const now = new Date().getTime();
              if (!videoStream?.['stream.start_time'] || now < videoStream?.['stream.start_time']) {
                videoStream['stream.status'] = 0;
              } else if (!videoStream?.['stream.end_time'] || videoStream?.['stream.end_time'] >= now) {
                videoStream['stream.status'] = 1;
              } else {
                videoStream['stream.status'] = 2;
              }
            })
            s[s.section_value] = result.filter(c => c?.['stream.status'] !== 2 || c?.['stream.video_transcode']?.length > 0)
          }
        } else if (s.section_value === 'brand_shop') {
          /** Logic hơi nhằng, nhưng đây là tách bạch ver cũ và mới
           * để khỏi bị if else quá nhiều trong 1 câu query
           */
          if (s.section_ref && isUid(s.section_ref)) {
            const {result} = await db.query(brandshopGroupQuery(customer_id, s.section_ref))
            let brandshopGroup_brandshops = result?.[0]?.['brandshop_group.brandshops'] || []
            for(let item of brandshopGroup_brandshops) {
              item.description = safeParseJson(item.description)
            }
            s[s.section_value] = brandshopGroup_brandshops
          } else {
            s[s.section_value] = []
          }
        } else if (s.section_value === 'promotions') {
          const {result} = await db.query(promotionQuery)
          s[s.section_value] = result ?? []
        }
      } else if (s.section_type && s.section_type === 'item') {
        if (s.section_value === 'collection_temp') {
          const {result: [data]} = await db.query(collectionTempQuery, {
            $section_ref: s.section_ref,
            $section_limit: String(s.section_limit || 20)
          })
          if (data) {
            // if (data.layout_type == 0) {
            //   data.layout_type = 'singleproduct'
            // } else if (data.layout_type == 1) {
            //   data.layout_type = 'multiproduct'
            // } else if (data.layout_type == 2) {
            //   data.layout_type = 'top10'
            // } else if (data.layout_type == 3) {
            //   data.layout_type = 'cuahang'
            // } else {
            //   data.layout_type = 'livestream'
            // }
            data.items = parseProduct(data.items)

            if (data.layout_type == 0) {
              // data.layout_type = 'singleproduct'
              data?.items?.filter(f => {
                if (f?.videos?.length) {
                  return f
                }
              })
            }
            s[s.section_value] = data
          }
        }
      } else if (s.section_type && s.section_type === 'list_item') {
        if (s.section_value === 'favourite' && customer_id) {
          const {result} = await db.query(favouriteQuery, {
            $customer_id: customer_id
          })

          const data = parseProduct(result)
          s[s.section_value] = data
        }
      }
      if (s.section_value && s.section_value === 'viewed_prod') {
        if (uids?.length) {
          const query = `{
            result(func: uid(${uids.join(',')}), orderdesc: created_at) ${prodFilter} {
              ${prodFields}
            }
          }`
          const {result} = await db.query(query)
          if (result?.length) {
            s[s.section_value] = parseProduct(result)
          }
        }
      }
      i++
    }
  }
  return data
}

module.exports = parseSections