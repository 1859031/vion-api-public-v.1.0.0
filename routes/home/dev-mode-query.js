const prodFragment = mrequire('./fragments/product')
const prodFilter = `@filter(NOT eq(is_deleted, true))`

const fragments =
`fragment collectionFields {
  uid
  collection_name
  collection_image
}
fragment productFields {
  ${prodFragment}
}`

const suggestQuery =
`Suggests(func: eq(collection_type, 200), orderasc: display_order) {
  count_products: count(highlight.products ${prodFilter})
  products: highlight.products @facets(orderasc: display_order) ${prodFilter}{
    ...productFields
  }
}`

const highlightQuery =
`HLCollections(func: eq(collection_type, 200), orderasc: display_order) @filter(eq(reference_type, 1)) {
  HighlightCollections: highlight.collections @facets(orderasc: display_order) @filter(eq(display_status, 2)) {
    ...collectionFields
    products: highlight.products @facets(orderasc: display_order) ${prodFilter}{
      ...productFields
    }
  }
}`

const collectionQuery =
`Collections(func: eq(collection_type, 0), orderasc: sort_order) @filter(NOT (has(parent) OR eq(is_deleted, true)) AND ( eq(display_status, 2) AND (NOT eq(is_temporary, true)) ) ) {
  ...collectionFields
  children: ~parent @filter(NOT eq(is_deleted, true)) @cascade {
    ...collectionFields
    items: ~product.collection ${prodFilter} {
      uid
      product_name
    }
  }
}`

const historyQuery = (uids) => `History(func: uid(${uids.join(',')}), orderdesc: created_at) ${prodFilter} {
  ...productFields
}`

const productHasVideo = `{
  var(func: eq(media_type, "video")) {
    ~previews {
      puid as uid
    }
  }
  Products(func: uid(puid), orderdesc: created_at, first: 20, offset: 0) ${prodFilter} {
    ...productFields
  }
}`

const allQuery = (uids) => `${fragments}
query {
  ${[suggestQuery, collectionQuery, historyQuery(uids), highlightQuery, productHasVideo].join("\n")}
}`
const noHistoryQuery =
`${fragments}
query {
  ${[suggestQuery, collectionQuery, highlightQuery, productHasVideo].join("\n")}
}`

module.exports = {
  allQueryDevMode: allQuery,
  noHistoryQueryDevMode: noHistoryQuery
}