const fs = require('fs')

module.exports = function (fastify, opts, done) {
  const routesStack = []
  const schemas = {}
  // Tự động đăng ký tất cả route có trong thư mục routers
  fs.readdirSync('./routes').forEach(function (name) {
    // Tìm các thư mục có chứa file index.js
    if(fs.existsSync(`./routes/${name}/index.js`)) {
      // Lấy các route được export trong index.js đưa vào fastify
      routesStack.push(require(`./${name}`))
    }
    if(fs.existsSync(`./routes/${name}/schema.js`)) {
      Object.assign(schemas, require(`./${name}/schema.js`))
    }
  })
  Promise.all(routesStack.map(routes => typeof routes === 'function' ? routes() : routes))
    .then(routesStack => {
      for(let routes of routesStack) {
        for(let r of routes) {
          if(Array.isArray(r)) {
            let [method, url, handler, opts = {}] = r
            if(schemas[url]) {
              opts.schema = schemas[url]
            }
            fastify[method.toLowerCase()]?.(url, {...opts, handler})
          } else {
            if(schemas[r.url]) {
              r.schema = schemas[r.url]
            }
            fastify.route(r)
          }
        }
      }
      done()
    }).catch(err => {
      log.error(err)
    })
}